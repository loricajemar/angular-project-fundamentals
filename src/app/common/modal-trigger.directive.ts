import { Directive, Inject, OnInit, ElementRef, Input } from "@angular/core";
import { JQ_TOKEN } from "./jQuery.service";

@Directive({
    selector: '[modal-trigger]'
})

export class ModalTriggerDirective implements OnInit {
    private el: HTMLElement
    @Input('modal-trigger') modalId: string
    @Inject(JQ_TOKEN) private $: any

    contructor(ref: ElementRef) {
        this.el = ref.nativeElement
    }

    ngOnInit() {
        this.el.addEventListener('click', e => {
            this.$(`#${this.modalId}`).modal({})
        })
    }

}