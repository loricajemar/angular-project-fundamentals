import { Routes } from "@angular/router";
import { PageNotFoundComponent } from "./errors/page-not-found.component";
import { 
    CreateEventComponent,
    EventDetailsComponent,
    EventRouteActivatorService,
    EventsListResolverService,
    EventsListComponent,
    CreateSessionComponent
} from './events/index';

export const appRoutes: Routes = [
    { path: 'events/new', component: CreateEventComponent, canDeactivate: ['canDeactivateCreateEvent'] },
    { path: 'events', component: EventsListComponent, resolve: {
         events: EventsListResolverService } },
    { path: 'events/:id', component: EventDetailsComponent,  canActivate: [EventRouteActivatorService] },
    { path: 'events/session/new', component: CreateSessionComponent },
    { path: '404', component: PageNotFoundComponent },
    { path: '', redirectTo: '/events', pathMatch: 'full' },
    { 
        path: 'user', 
        loadChildren: () => import('./user/user.module')
        .then(m => m.UserModule)
    }
]