import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { EventService } from "./index";

@Component({
    // selector: 'create-event',
    templateUrl: './create-event.component.html',
    styleUrls: ['./create-event.component.css']
})

export class CreateEventComponent {
    newEvent
    isDirty: boolean = true

    constructor(
        private router: Router,
        private eventService: EventService) { }

    saveEvent(formValues) {
        this.eventService.saveEvent(formValues)
        this.isDirty = false
        this.router.navigate(['/events'])
    }
    
    cancel() {
        this.router.navigate(['/events'])
    }
    
}

export function checkDirtyState(component: CreateEventComponent) {
    if(component.isDirty) {
        return window.confirm('You have unsaved event. Do you really want to cancel?')
    }
    return true
    
}
